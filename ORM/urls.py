from django.urls import path
from . import views

urlpatterns = [
    path('graph1/', views.graph1, name='graph1'),
    path('graph2/', views.graph2, name='graph2'),
    path('graph3/', views.graph3, name='graph3'),
    path('graph4/', views.graph4, name='graph4'),
    path('graph5/', views.graph5, name='graph5'),
    path('ipl1/', views.ipl1, name='ipl1'),
    path('ipl2/', views.ipl2, name='ipl2'),
    path('ipl3/', views.ipl3, name='ipl3'),
    path('ipl4/', views.ipl4, name='ipl4'),
    path('ipl5/', views.ipl5, name='ipl5'),
]