# Generated by Django 2.1.5 on 2019-01-14 17:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Deliveries',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inning', models.TextField()),
                ('batting_team', models.TextField()),
                ('bowling_team', models.TextField()),
                ('over', models.TextField()),
                ('ball', models.TextField()),
                ('batsman', models.TextField()),
                ('non_striker', models.TextField()),
                ('bowler', models.TextField()),
                ('is_super_over', models.TextField()),
                ('wide_runs', models.TextField()),
                ('bye_runs', models.TextField()),
                ('legbye_runs', models.TextField()),
                ('noball_runs', models.TextField()),
                ('penalty_runs', models.TextField()),
                ('batsman_runs', models.TextField()),
                ('extra_runs', models.TextField()),
                ('total_runs', models.TextField()),
                ('player_dismissed', models.TextField()),
                ('dismissal_kind', models.TextField()),
                ('fielder', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Matches',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('match_id', models.TextField()),
                ('season', models.TextField()),
                ('city', models.TextField()),
                ('date', models.TextField()),
                ('team1', models.TextField()),
                ('team2', models.TextField()),
                ('toss_winner', models.TextField()),
                ('toss_decision', models.TextField()),
                ('result', models.TextField()),
                ('dl_applied', models.TextField()),
                ('winner', models.TextField()),
                ('win_by_runs', models.TextField()),
                ('win_by_wickets', models.TextField()),
                ('player_of_match', models.TextField()),
                ('venue', models.TextField()),
                ('umpire1', models.TextField()),
                ('umpire2', models.TextField()),
                ('umpire3', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='deliveries',
            name='deliveries_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='custom_command.Matches'),
        ),
    ]
