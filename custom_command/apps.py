from django.apps import AppConfig


class CustomCommandConfig(AppConfig):
    name = 'custom_command'
