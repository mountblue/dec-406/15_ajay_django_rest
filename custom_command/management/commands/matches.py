from django.core.management.base import BaseCommand, CommandError
from custom_command.models import  Matches
import csv

class Command(BaseCommand):

    # def add_arguments(self, parser):
    #     parser.add_argument('file_name', nargs='+', type=str)

    def handle(self, *args, **options):
         with open('matches.csv', 'r') as file:
           matches_reader = csv.DictReader(file)
           for match in matches_reader:
               match_dict = dict(match)
               _, created = Matches.objects.update_or_create(
               match_id = match_dict['id'], 
               season = match_dict['season'], 
               city = match_dict['city'], 
               date = match_dict['date'], 
               team1 = match_dict['team1'], 
               team2 = match_dict['team2'], 
               toss_winner = match_dict['toss_winner'], 
               toss_decision = match_dict['toss_decision'], 
               result = match_dict['result'], 
               dl_applied = match_dict['dl_applied'],
               winner = match_dict['winner'], 
               win_by_runs = match_dict['win_by_runs'], 
               win_by_wickets = match_dict['win_by_wickets'],
               player_of_match = match_dict['player_of_match'], 
               venue = match_dict['venue'], 
               umpire1 = match_dict['umpire1'],
               umpire2 = match_dict['umpire2'], 
               umpire3 = match_dict['umpire3'])